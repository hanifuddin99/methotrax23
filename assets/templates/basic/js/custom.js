$(document).ready(function(){
// ======== responsiv navbar ========
let scrollheader = document.querySelector('.custo-nav');
let navBars = document.getElementsByClassName("fa-bars")[0];
let nvShow = document.querySelector(".nav-right");
navBars.addEventListener('click', (e) => {
    if (nvShow.style.visibility == "visible") {
        nvShow.style.visibility = "hidden";
    } else {
        nvShow.style.opacity = "1";
        nvShow.style.visibility = "visible";
    }
});

window.addEventListener('scroll', () => {
    scrollheader.classList.toggle('sticky', window.scrollY > 0);
});

// ======== faq =============
let questions = document.querySelectorAll('.questions');

questions.forEach((question, i) => {
    let label = question.querySelector('label');
    let content = question.querySelector('.content');
    let showMore = question.querySelector('.showMore');
    let shortContents = content.querySelector('.short-contents');
    let fullContents = content.querySelector('.full-contents');
    let strLen = showMore.getAttribute('data-id');

    label.addEventListener('click', (e) => {
        if (content.classList.contains('detailsContent')) {
            content.classList.remove('detailsContent');
            shortContents.style.display = "block";
            fullContents.style.display = "none";
        }
        question.classList.toggle('active');
        if (question.classList.contains('active')) {
            if (strLen > 1000) {
                showMore.style.display = 'block';
            }
            showMore.addEventListener('click', (e) => {
                shortContents.style.display = "none";
                fullContents.style.display = "block";
                content.classList.add('detailsContent');
                if (showMore.style.display == "block") {
                    showMore.style.display = 'none';
                }
            });
        }
        removeActive(i);
    });
});

function removeActive(i) {
    questions.forEach((questionForRemove, index) => {
        if (i != index) {
            questionForRemove.classList.remove('active');
        }
    });
}


}); // end of document ready function;