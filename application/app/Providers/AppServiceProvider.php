<?php

namespace App\Providers;

use Illuminate\Http\Request;
use App\Models\VisitorCounter;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(Request $request): void
    {
        
        date_default_timezone_set('Asia/Dhaka');
        $visitor_ip = $request->ip();
        $today = date('Y-m-d');
        $visitorResult = VisitorCounter::firstOrCreate([
            'visitors_ip' => $visitor_ip,
            'created_at' => $today
            ])->count();
        View::share('visitorResultCount', number_format($visitorResult));    
    }
}
