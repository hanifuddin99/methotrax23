<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BariNavMenu;
use App\Models\UpaNavMenu;
use App\Models\TofaNavMenu;
use App\Models\BariFaq;
use App\Models\UpaFaq;
use App\Models\TofaFaq;

class HomeController extends Controller
{
    // baricit =============
    public function indexBari(){
        $faqs = BariFaq::get();
        $navMenus = BariNavMenu::get();
        return view('templates.basic.baricit',['faqs' => $faqs, 'navMenus' => $navMenus]);
    }
    // upacit =============
    public function indexUpa(){
        $faqs = UpaFaq::get();
        $navMenus = UpaNavMenu::get();
        return view('templates.basic.upacit',['faqs' => $faqs, 'navMenus' => $navMenus]);
    }

    // tofacit ==========
    public function indexTofa(){
        $faqs = TofaFaq::get();
        $navMenus = TofaNavMenu::get();
        return view('templates.basic.tofacit',['faqs' => $faqs,'navMenus' => $navMenus]);
    }
    
}
