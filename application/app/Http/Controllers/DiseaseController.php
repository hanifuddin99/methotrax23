<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DiseaseController extends Controller
{
    protected $possibleModels = [
        'RheumatoidArthritisFaq' => \App\Models\RheumatoidArthritisFaq::class,
        'AnkylosingSpondylitisFaq' => \App\Models\AnkylosingSpondylitisFaq::class,
        'LupusFaq' => \App\Models\LupusFaq::class,
    ];    
    public function index($slug){ 
        $model_name = str_replace("_", "", ucwords($slug, '_'));
        $model = $this->possibleModels[$model_name];
        // use above code or bellow code for dynamic model 
        /* $model = "'\App\Models\'". $model_name;
        $model = str_replace('\'','',$model); */
        $faqs = $model::get();
        return view('templates.basic.'.$slug,['faqs' => $faqs]);
    }
}
