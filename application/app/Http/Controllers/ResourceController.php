<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResourceController extends Controller
{
    public function exercise(){
        return view('templates.basic.exercise');
    }

    public function feedback(){
        return view('templates.basic.feedback');
    }
}
