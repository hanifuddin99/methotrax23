@extends('templates.basic.layouts.frontend')
@section('content')




<div class="comment-area mt-4">
    <div class="card card-body">
        <h2 class="card-title">Leave a comment</h2>
        <form action="{{url('comments')}}" method="post">
            @csrf
            <input type="text" value="">
            <textarea rows="3" name="comment_body" id="comment_body" class="form-control" required></textarea>
            <button type="submit" class="btn btn-primary mt-3" data-bs-toggle="modal" data-bs-target="#loginModal">Submit</button>
        </form>
    </div>

    <div class="card card-body shadow-sm mt-3">
        <div class="detail-area">
            <h6 class="user-name mb-1">
                User One
                <small class="mb-3 text-primary">Commented on: 9-22-2024</small>
            </h6>
            <p class="user-comment mb-1">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo officia dolorem quam.
            </p>
        </div>
        <div>
            <a href="" class="btn btn-primary btn-sm">Edit</a>
            <a href="" class="btn btn-primary btn-sm">Delete</a>
        </div>
    </div>
</div>



<!-- google login modal -->

<div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="loginModalModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalModalLabel">Login for Comments</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Please login 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Login with Google</button>
      </div>
    </div>
  </div>
</div>

@endsection


