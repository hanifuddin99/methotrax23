@extends('templates.basic.layouts.frontend')
@section('content')

<!-- 20 years logo -->
<div class="popup-container" id="popup">
    <div class="popup" data-text="&#x274c;">
        <img src="{{asset('assets/images/frontend/20_years_logo.jpg')}}">
    </div>
</div>
<!-- End 20 years logo -->

<!---------------- banner -------------------->
<div class="bnner-sec-hm">
    <img src="{{ asset('assets/templates/basic/images/dmard_home.png') }}">
</div>
<!---------------- End banner -------------------->
<!-- ------------ content ------ -->
<section class="content-sect">
    <div class="content-container">
        <h2>About</h2>
        <img class="under-title-line" src="{{asset('assets/templates/basic/images/upa-line.png')}}">
        <div class="contentBx">
            <div class="content">
                <div class="cnt-inner">
                    <p>
                        Welcome to Delta Pharma's dedicated website on Disease-Modifying Antirheumatic Drugs (DMARDs),
                        where we proudly showcase our comprehensive portfolio of treatments designed to improve the
                        lives of
                        individuals battling rheumatic diseases. Since our inception, Delta Pharma has been at the
                        forefront
                        of pioneering healthcare solutions, introducing groundbreaking DMARD therapies to meet the needs
                        of
                        patients across Bangladesh. Among our esteemed lineup, Methotrax stands as a testament to our
                        commitment to excellence,
                    </p>
                    <p>
                        having been the First-Time in Bangladesh brand since its landmark launch
                        in 2004. Our dedication to innovation continued with the introduction of Reumaflex in 2016,
                        followed by Tofacit in 2018 and Baricit in 2020, each representing significant milestones in
                        our journey towards enhancing patient care. As we forge ahead, we remain steadfast in our
                        mission
                        to deliver cutting-edge treatments, exemplified by the last addition of Upacit XR to our
                        esteemed
                        DMARD portfolio. At Delta Pharma, we are committed to highest quality of product at an
                        affordable
                        price.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ------------ End content ------ -->

@endsection

@push('scripts')
    <script>

        let popup = document.querySelector('#popup');
        let clasPopup = document.querySelector('.popup');

        setTimeout(() => {
            clasPopup.classList.add('popup2');
        }, 100);
        setTimeout(() => {
            clasPopup.style.height = 0;
            popup.style.height = 0;
            setTimeout(() => {popup.style.display = "none";},4500);
        }, 8000);
        clasPopup.onclick = () => {
            clasPopup.style.height = 0;
            popup.style.height = 0;
            setTimeout(() => {popup.style.display = "none";},4500);
        }
    </script>
@endpush