<div class="custo-nav">
    <div class="header-brand">
        <a href="{{url('https://www.deltapharmabd.com/')}}">
            <img src="{{asset('assets/templates/basic/images/logo.gif')}}">
        </a>
    </div>
    <div class="nav-right">
        <ul>
            <li><a class="active" href="{{url('/')}}">Home</a></li>
            <li>
                <a href="javascript:void(0)">Products</a>
                <ul>
                    <li>
                        <a href="{{route('baricit')}}">
                            Baricit
                        </a>
                    </li>
                    <li>
                        <a href="{{route('tofacit')}}">
                            Tofacit
                        </a>
                    </li>
                    <li>
                        <a href="{{route('upacit')}}">
                            Upacit
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)">Disease Profile</a>
                <ul>
                    <li>
                        <a href=" {{route('disease',['slug' => 'Rheumatoid_arthritis_faq'])}} ">
                            Rheumatoid Arthritis
                        </a>
                    </li>
                    <li>
                        <a href=" {{route('disease',['slug' => 'Ankylosing_spondylitis_faq'])}} ">
                            Ankylosing Spondylitis
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Psoriasis And Psoriatic Arthritis
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('disease',['slug' => 'Lupus_faq']) }}">
                            Lupus
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)">Patient Resources</a>
                <ul>
                    <li>
                        <a href="{{ route('execise') }}">
                            Exercise
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Food
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Lifestyle
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('feedback')}}">Feedback</a>
            </li>
            <li>
                <a href="javascript:void(0)">Contact Us</a>
            </li>
        </ul>
    </div>
    <button><i class="fa-solid fa-bars"></i></button>
</div>
<script>
// delta logo shake
let shake = document.querySelector(".header-brand a");
setInterval(() => {
    shake.classList.remove("shake");
}, 3500);
</script>