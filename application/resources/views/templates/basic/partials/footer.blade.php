<div class="footer-cont">
    <div class="related-lnks">
        <h3>Related Links</h3>
        <div>
            <ul>
                <li>
                    <a href="{{route('baricit')}}">baricit</a>
                </li>
                <li>
                    <a href="{{route('tofacit')}}">tofacit</a>
                </li>
                <li>
                    <a href="{{route('upacit')}}">upacit</a>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="{{route('baricit')}}">exercise</a>
                </li>
                <li>
                    <a href="{{route('tofacit')}}">food</a>
                </li>
                <li>
                    <a href="{{route('upacit')}}">lifestyle</a>
                </li>
            </ul>

        </div>
    </div>
    <div class="dpl-lnk">
        <ul>
            <li>
                <a href="{{route('baricit')}}">rheumatoid arthritis</a>
            </li>
            <li>
                <a href="{{route('tofacit')}}">ankylosing spondylitis</a>
            </li>
            <li>
                <a href="{{route('upacit')}}">psoriasis and psoriatic arthritis</a>
            </li>
            <li>
                <a href="{{route('upacit')}}">lupus</a>
            </li>
        </ul>
        <a href="{{url('https://deltapharmabd.com/')}}" target="_blank">
            <img src="{{asset('assets/templates/basic/images/logo.gif')}}">
        </a>
    </div>
    <div class="social">
        <h3>Follow Us</h3>
        <div class="social-icons">
            @php $urlSegOne = Request::segment(1); @endphp
            @if($urlSegOne == "baricit")
            <a href="https://www.facebook.com/Baricit2" target="_blank"><i class="fa-brands fa-facebook-f"></i></a>
            @elseif($urlSegOne == "tofacit")
            <a href="https://www.facebook.com/Tofacitinib.Tofacit" target="_blank"><i
                    class="fa-brands fa-facebook-f"></i></a>
            @elseif($urlSegOne == "upacit")
            <a href="https://www.facebook.com/Upadacitinib" target="_blank"><i class="fa-brands fa-facebook-f"></i></a>
            @else
            <a href="https://www.facebook.com/DeltaPharmaLimited" target="_blank"><i
                    class="fa-brands fa-facebook-f"></i></a>
            @endif
            <a href="{{ url('https://www.youtube.com/@DeltaPharmaLimited') }}" target="_blank"><i
                    class="fa-brands fa-square-youtube"></i></a>
            <a href="{{ url('https://wa.me/01713335550') }}" target="_blank"><i class="fa-brands fa-whatsapp"></i></a>
        </div>
        <div class="visitor-counter">
            <h4>{{"Visited:"}}&nbsp <span>{{$visitorResultCount}}</span></h4>
        </div>
    </div>
</div>
<div class="last-foot">
    <!-- <p>Copyright&copy; Delta Pharma Limited. All rights reserved.</p> -->
</div>