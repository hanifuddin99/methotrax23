<!doctype html>
<html lang="en" itemscope itemtype="http://schema.org/WebPage">

<head>
    <!-- Required meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="og:type" content="website" />
    <meta name="description"
        content="Methotrax Tablet(মিথোট্রেক্স ১০ মি.গ্রা.) -- DMARD Products  are very useful for DMARD patient, product of Delta Pharma Ltd. Methotrexate is generic name.">
    <meta property="og:title" content="Methotrax Tablet 10 mg (মিথোট্রেক্স ১০ মি.গ্রা.)">
    <title> {{ "Methotrax Tablet-Dmard-মিথোট্রেক্স ১০ ট্যাবলেট-Delta Pharma Ltd.  " }}</title>
    <!-- company icon -->
    <link rel='icon' href='{{ asset("assets/templates/basic/images/icon-logo.gif") }}' sizes='16x16'>
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
        integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- bootstrap css -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- 'aos' plugin animate css, scrolling effect -->
    <link href="{{ asset('assets/templates/basic/css/animate-css/aos.css') }}" rel="stylesheet">
    <!-- main style css link -->
    <link rel="stylesheet" href="{{ asset('assets/templates/basic/css/mystyle.css')}}">
</head>

<body>
    <!-- == header == -->
    @include("templates.basic.partials.header")
    <!-- == End header == -->

    @yield('content')

    <!-- ------------ footer ------ -->
    @include('templates.basic.partials.footer')
    <!-- ------------ End footer ------ -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- jquery plugins -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"
        integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- bootstrap js bundel -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <!-- 'aos' plugin animate js, scrolling effect  -->
    <script src="{{ asset('assets/templates/basic/js/aos.js') }}"></script>
    <!-- 'custom js' for responsive navigation, faq effect  -->
    <script src="{{ asset('assets/templates/basic/js/custom.js') }}"></script>

    @stack('scripts')
    <script>
    $(document).ready(function() {
        // aos (js animation) initialization
        AOS.init();
    });
    </script>

</body>

</html>