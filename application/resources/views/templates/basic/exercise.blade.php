@extends('templates.basic.layouts.frontend')
@section('content')
<!---------------- banner -------------------->
<div class="bnner-sec-hm">
    <img src="{{ asset('assets/templates/basic/images/dmard_home.png') }}">
</div>
<!---------------- End banner -------------------->

<!-- ------------ content ------ -->
<section id="excercise-section" class="excercise-container">
    <h1>গুরুত্বপূর্ণ ব্যায়ামসমূহ</h1>
    <img class="under-title-line" src="{{asset('assets/templates/basic/images/bari-line.png')}}">
    <div class="exercise-downld-area">
        <button>
            Download Exercise
        </button>
        <div class="exercise-downld">
            <span>X</span>
            <div class="dwnld-content">
                <div class="dwnld-cntnt-file">
                    <a href="javascript:void(0);">স্পনডাইলো আর্থ্রাইটিস এবং এনকাইলোজিং স্পন্ডিলাইটিস এর ব্যায়াম সমূহ</a>
                    <a href="{{ asset('assets/images/frontend/files/ankylosing_spond_exercise.pdf') }}" download>
                        <i class="fa-solid fa-download"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="excercise-wrapper">
        <div class="excer-cover excer-cover-left"></div>
        <div class="excer-cover excer-cover-right"></div>

        <div class="excer-book">
            <div class="excer-book-page page-left">
                <img src="{{asset('assets/images/frontend/arthritis/exercise/ankylos_exercise1.jpg')}}"
                    alt="{{'Ankylosing arthritis'}}">
            </div>
            <!-- page 1 & 2  -->
            <div class="excer-book-page page-right turn" id="turn-1">
                <!-- page 1 -->
                <div class="page-front">
                    <img src="{{asset('assets/images/frontend/arthritis/exercise/ankylos_exercise2.jpg')}}"
                        alt="{{'Ankylosing arthritis'}}">
                    <!-- front page -->
                    <span class="number-page">1</span>
                    <span class="nextPrev-btn" data-page="turn-1">
                        <i class="fa-solid fa-chevron-right"></i>
                    </span>
                </div>
                <!-- page 2  -->
                <div class="page-back">
                    <!-- <img src="{{asset('assets/images/frontend/arthritis/exercise/ankylos_exercise2.jpg')}}"
                        alt="{{'Ankylosing arthritis'}}"> -->
                    <!-- back page -->
                    <span class="number-page">2</span>
                    <span class="nextPrev-btn back" data-page="turn-1">
                        <i class="fa-solid fa-chevron-left"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ------------ End content ------ -->

<script>
// download exercise  
const btn = document.querySelector(".exercise-downld-area button");
const dwnDiv = document.querySelector(".exercise-downld");
const dwnldDvCls = document.querySelector('.exercise-downld span');

// download button visible
window.addEventListener('scroll', function() {
    let scrlVrtical = window.scrollY;
    if (scrlVrtical > 400) {
        btn.style.visibility = "visible";
        btn.style.opacity = 1;
        btn.style.transition = "opacity 2s linear";
    }
});

// download div visible
btn.addEventListener('click', function() {
    dwnDiv.style.visibility = "visible";
    dwnDiv.style.opacity = 1;
    dwnDiv.style.transition = "opacity 1s linear";
});

dwnldDvCls.addEventListener('click', function() {
    dwnDiv.style.visibility = "hidden";
    dwnDiv.style.opacity = 0;
    dwnDiv.style.transition = "opacity 2s linear";
});
//  for file animation   
// turn cover right side automatic with animation
const coverTurnRight = document.querySelector(".excer-cover.excer-cover-right");
setTimeout(() => {
    coverTurnRight.classList.add('turn');
}, 5100);
// stable the last cover
setTimeout(() => {
    coverTurnRight.style.zIndex = -1;
}, 5900);
// fit the middle of fold of the covers
const pageLeft = document.querySelector('.excer-book-page.page-left');
setTimeout(() => {
    pageLeft.style.zIndex = 20;
}, 6000);
const exerBookPages = document.querySelectorAll('.excer-book-page.page-right');
const totalPages = exerBookPages.length;

let pageNumber = 0;

function reverseIndex() {
    pageNumber--;
    if (pageNumber < 0) {
        pageNumber = totalPages - 1;
    }
}

exerBookPages.forEach((_, index) => {
    setTimeout(() => {
        reverseIndex();
        exerBookPages[pageNumber].classList.remove('turn');
        setTimeout(() => {
            reverseIndex();
            exerBookPages[pageNumber].style.zIndex = 10 + index;
        }, 500);
    }, (index + 1) * 200 + 5100);
});


const nextPrevBtn = document.querySelectorAll('.nextPrev-btn');
nextPrevBtn.forEach((el, index) => {
    el.onclick = () => {
        const pageTurnId = el.getAttribute('data-page');
        const pageTurn = document.getElementById(pageTurnId);
        if (pageTurn.classList.contains('turn')) {
            pageTurn.classList.remove('turn');
            setTimeout(() => {
                pageTurn.style.zIndex = 20 - index;
            }, 500);
        } else {
            pageTurn.classList.add('turn');
            setTimeout(() => {
                pageTurn.style.zIndex = 20 + index;
            }, 500);
        }
    }
});
</script>
@endsection