@extends('templates.basic.layouts.frontend')
@section('content')
<!---------------- banner -------------------->
<div class="bnner-sec-hm">
    <img src="{{ asset('assets/templates/basic/images/dmard_home.png') }}">
</div>
<!---------------- End banner -------------------->

<!-- ------------ faq ------ -->
<div class="faq-container" id="about">
    <h1>আপনার গুরুত্বপূর্ণ প্রশ্নের উত্তর</h1>
    <img class="under-title-line" src="{{asset('assets/templates/basic/images/upa-line.png')}}">
    <div class="accor-contai">
        <ul class="accordion">
            @foreach($faqs as $faq)
            <li class="questions" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">
                <label for="questions">{{$faq->questions}}<span>&#x3e</span></label>
                <div class="content">
                    @php
                    $contents = substr($faq->answers, 0);
                    $contents_len = strlen($contents);
                    $short_contents = preg_replace('/\s+?(\S+)?$/', '', substr($faq->answers, 0, 800))
                    @endphp
                    <div class="short-contents">
                        {!! $short_contents !!}
                    </div>
                    <div class="showMore" data-id="{{ $contents_len }}">
                        <a href="javascript:void(0)">বিস্তারিত</a>
                    </div>
                    <div class="full-contents">
                        {!! $faq->answers !!}
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
<!-- ===============End faq =========== -->
@endsection