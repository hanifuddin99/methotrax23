@extends('templates.basic.layouts.frontend')
@section('content')
<div class="upa-container">
    <!------------- banner image-------- -->
    <div class="bnner-sec-hm">
        <img src="{{ asset('assets/templates/basic/images/tofacit_banner.png') }}">
    </div>
    <!------------- End banner image-------- -->

    <!-- -------------- Faq ------------ -->

    <div class="faq-container" id="faq">
        <h1>আপনার গুরুত্বপূর্ণ প্রশ্নের উত্তর</h1>
        <img class="under-title-line" src="{{asset('assets/templates/basic/images/tofa-line.png')}}">
        <div class="accor-contai">
            <ul class="accordion">
                @foreach($faqs as $faq)
                <li class="questions" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">
                    <label for="questions">{{$faq->questions}}<span>&#x3e</span></label>
                    <div class="content">
                        @php
                        $contents = substr($faq->answers, 0);
                        $contents_len = strlen($contents);
                        $short_contents = preg_replace('/\s+?(\S+)?$/', '', substr($faq->answers, 0, 1000))
                        @endphp
                        <div class="short-contents">
                            {!! $short_contents !!}
                        </div>
                        <div class="showMore" data-id="{{ $contents_len }}">
                            <a href="javascript:void(0)">বিস্তারিত</a>
                        </div>
                        <div class="full-contents">
                            {!! $faq->answers !!}
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <!-- -------------- End Faq ------------ -->
    <!-- ============== popup msg to patient =========== -->
    <div class="purchase-popup">
        <img src="{{asset('assets/images/frontend/medicine_pop_up.jpg')}}">
        <span>X</span>
    </div>
    <!-- ============== End popup msg to patient =========== -->
</div>
<script>
//  popup msg to patient
const prchse = document.querySelector('.purchase-popup');
const prchseCls = document.querySelector('.purchase-popup span');

prchseCls.addEventListener('click', () => {
    prchse.style.display = "none";
});
setTimeout(function() {
    prchse.style.display = "block";
}, 300);
</script>
@endsection