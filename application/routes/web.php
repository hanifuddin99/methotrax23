<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('templates.basic.home');
})->name('home');



Route::controller('HomeController')->group(function(){
    // ====== baricit =====
    Route::prefix('baricit')->group(function(){
        Route::get('/','indexBari')->name('baricit');
    });
    // ====== upacit =====
    Route::prefix('upacit')->group(function(){
        Route::get('/', 'indexUpa')->name('upacit');
    });

    // ====== tofacit =======

    Route::prefix('tofacit')->group(function(){
        Route::get('/', 'indexTofa')->name('tofacit');
    });
});


// ==== Disease =========== 
Route::controller('DiseaseController')->prefix('disease')->group(function(){
    Route::get('{slug}','index')->name('disease');
});

// ======== Resource ===========
// here in exercise, feedback
Route::controller("ResourceController")->prefix('resource')->group(function(){
    Route::get('exercise','exercise')->name('execise');
    Route::get('feedback','feedback')->name('feedback');
});

// ============ Comments ================

Route::controller("CommentController")->group(function() {
    Route::get('loginToComment','loginToComment')->name('loginToComment');
    Route::post('comments','commentsstore')->name('comments');
});