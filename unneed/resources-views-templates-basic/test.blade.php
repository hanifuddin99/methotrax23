@extends('templates.basic.layouts.frontend')
@section('content')
<style>
.upa-nav button {
    position: relative;
    display: none;
    width: 40px;
    height: 30px;
    background: var(--title-color);
    cursor: pointer;
    transition: 0.5s all;
}

.upa-nav button i {
    position: absolute;
    top: 55%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 100%;
    height: 100%;
    color: var(--white-font-color);
    font-size: 20px;
}

.upa-nav button:hover {
    background: var(--foot-bg-color);
}

@media screen and (max-width: 1024px) {
    .upa-nav {
        padding: 10px 5px;
    }

    .upa-nav .nav-right {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100vh;
        background: #f9f9fb;
        visibility: hidden;
        opacity: 0;
        flex-direction: column;
        transition: 0.4s opacity;
    }

    .upa-nav .nav-right a {
        width: 40%;
    }

    .upa-nav .nav-right a span {
        margin-right: 2px;
    }

    .upa-nav button {
        position: relative;
        display: block;
    }
}
</style>
<div class="upa-container">
    <div class="upa-nav">
        <div class="foot-logo-cont">
            <h1>dmard</h1>
            <a href="{{url('/')}}" class="foot-logo">
                <img src="{{asset('assets/templates/basic/images/upacit_logo.png')}}">
            </a>
        </div>
        <div class="nav-right">
            <a href="#about">
                <span>01.</span> About
            </a>
            <a href="#faq">
                <span>02.</span> FAQ
            </a>
            <a href="{{route('tofacit')}}">
                <span>03.</span> Tofacit
            </a>
        </div>
        <button><i class="fa-solid fa-bars"></i></button>
    </div>

    <script>
    let navBars = document.getElementsByClassName("fa-bars")[0];
    let nvShow = document.querySelector(".nav-right");
    navBars.addEventListener('click', (e) => {
        if (nvShow.style.visibility == "visible") {
            nvShow.style.visibility = "hidden";
        } else {
            nvShow.style.opacity = "1";
            nvShow.style.visibility = "visible";
        }
    });

    window.addEventListener('scroll', () => {

        if (screen.width < 1024) {
            nvShow.style.visibility = "hidden";
        }
    });
    </script>

    <!------------- banner image-------- -->
    <div class="bnner-sec">
        <div class="bnner-img"></div>
    </div>
    <!-- -------------- Faq ------------ -->

    <div class="faq-container" id="faq">
        @php
        $new = htmlspecialchars("<a href='test'>Test</a>");
            echo $new;
        @endphp
        <h1>আপনার গুরুত্বপূর্ণ প্রশ্নের উত্তর</h1>
        <img src="{{asset('assets/images/frontend/Line-01.png')}}">
        <div class="accor-contai">
            <ul class="accordion">
                @foreach($faqs as $faq)
                <li class="questions" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">
                    <label for="questions">{{$faq->questions}}<span>&#x3e</span></label>
                    <div class="content">
                        @php
                            $contents = substr($faq->answers, 0);
                            $contents_len = strlen($contents);
                            $short_contents = preg_replace('/\s+?(\S+)?$/', '', substr($faq->answers, 0, 1000))
                        @endphp
                        <div class="short-contents">
                            {!! $short_contents !!}
                        </div>
                        <div class="showMore" data-id="{{ $contents_len }}">
                            <a href="javascript:void(0)">বিস্তারিত</a>
                        </div>
                        <div class="full-contents">
                            {!! $faq->answers !!}
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    @include('templates.basic.partials.footer')
</div>

<script>
let questions = document.querySelectorAll('.questions');

questions.forEach((question, i) => {
    let label = question.querySelector('label');
    let content = question.querySelector('.content');
    let showMore = question.querySelector('.showMore');
    let shortContents = content.querySelector('.short-contents');
    let fullContents = content.querySelector('.full-contents');
    let strLen = showMore.getAttribute('data-id');

    label.addEventListener('click', (e) => {
        if (content.classList.contains('detailsContent')) {
            content.classList.remove('detailsContent');
            shortContents.style.display = "block";
            fullContents.style.display = "none";
        }
        question.classList.toggle('active');
        if (question.classList.contains('active')) {
            if(strLen > 1000){
            showMore.style.display = 'block';
            }
            showMore.addEventListener('click', (e) => {
                shortContents.style.display = "none";
                fullContents.style.display = "block";
                content.classList.add('detailsContent');
                if (showMore.style.display == "block") {
                    showMore.style.display = 'none';
                }
            });
        }
        removeActive(i);
    });
});

function removeActive(i) {
    questions.forEach((questionForRemove, index) => {
        if (i != index) {
            questionForRemove.classList.remove('active');
        }
    });
}
</script>

@endsection