<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('upacit-test/',function(){
    return view('templates.basic.upacit_copy');
});

Route::controller('HomeController')->group(function(){
    
// for testing purpose
    Route::get('/test', 'test');
});
